[0.4.0]
- Update to new version 0.13.0
- Enable email sending

[0.5.0]
- Update to new version 0.14.0
- Increase minimum memory requirement

[0.6.0]
- Update to new version 0.16.0
- Fix the login form button text

[0.6.1]
- Use release packages
- Fix user data mapping from ldap

[0.6.2]
- Fix Site URL

[0.7.0]
- Update to new version 0.20.0

[0.8.0]
- Update to new version 0.21.0

[0.9.0]
- Update to new version 0.22.0

[0.10.0]
- Update to new version 0.23.0
- Fix issue where an error was displayed when creating a web integration
- Enable imagemagick support

[0.11.0]
- Update to latest version 0.24.0
- Fix url of logo asset

[0.12.0]
- Update to latest version 0.26.0
- Allow email login

[0.12.1]
- Update to Rocket.Chat 0.27.0

[0.12.2]
- Update to Rocket.Chat 0.29.0

[0.12.3]
- Use latest SMTP settings

[0.12.4]
- Update Rocket.Chat to 0.35.0

[0.12.5]
- Update Rocket.Chat to 0.36.0

[0.13.0]
- Update Rocket.Chat to 0.37.1

[0.13.1]
- Update Rocket.Chat to 0.39.0

[0.13.2]
- Add post install message

[0.13.3]
- Update Rocket.Chat to 0.42.0

[0.14.0]
- Update Rocket.Chat to 0.45.0

[0.15.0]
- Update Rocket.Chat to 0.47.0
- Support optional SSO

[0.16.0]
- Update Rocket.Chat to 0.49.3

[0.17.0]
- Update Rocket.Chat to 0.52.0

[0.18.0]
- Update Rocket.Chat to 0.53.0
- Better post install message

[0.19.0]
- Update Rocket.Chat to 0.54.2

[0.20.0]
- Update Rocket.Chat to 0.55.1

[0.21.0]
- Update Rocket.Chat to 0.56.0

[0.22.0]
- Update Rocket.Chat to 0.57.0

[0.22.1]
- Update Rocket.Chat to 0.57.1

[0.22.2]
- Update Rocket.Chat to 0.57.2

[1.0.0]
- Update Rocket.Chat to 0.57.3

[1.1.0]
- Update Rocket.Chat to 0.58.0
- #7479 Add admin and user setting for notifications #4339
- #7529 Add close button to flex tabs
- #7444 Fix Anonymous User
- #7334 Search users also by email in toolbar
- Read complete list of changes at https://github.com/RocketChat/Rocket.Chat/releases/tag/0.58.0

[1.1.1]
- Update Rocket.Chat to 0.58.1
- #7781 Fix flex tab not opening and getting offscreen

[1.1.2]
- Update Rocket.Chat to 0.58.2
- #7758 Fixed bug preventing validation emails to be sent

[1.1.3]
- Update Rocket.Chat to 0.58.3

[1.1.4]
- Update Rocket.Chat to 0.58.4
- #8408 Duplicate code in rest api letting in a few bugs with the rest api
- #8390 Slack import failing and not being able to be restarted

[1.2.0]
- Update Rocket.Chat to 0.59.3

[1.2.1]
- Update Rocket.Chat to 0.59.3
- This version requires Cloudron version 1.7.6 due to LDAP fixes

[1.3.0]
- Update Rocket.Chat to 0.59.4
- Add documentationUrl

[1.4.0]
- Fix issue where uploads stop working after sometime

[1.5.0]
- Update Rocket.Chat to 0.60.1
- #8915 Add "Favorites" and "Mark as read" options to the room list
- #8739 Add "real name change" setting
- #8626 Add icon art in Tokenpass channel title
- #8947 Add new API endpoints
- #8304 Add RD Station integration to livechat
- #8066 Add settings for allow user direct messages to yourself
- #8108 Add sweet alert to video call tab
- #8037 Add yunohost.org installation method to Readme.md
- #8902 Added support for Dataporten's userid-feide scope
- #7641 Adds admin option to globally set mobile devices to always be notified regardless of presence status.
- #7285 Allow user's default preferences configuration
- #8857 code to get the updated messages
- #8924 Describe file uploads when notifying by email
- #8143 Displays QR code for manually entering when enabling 2fa
- #8260 Enable read only channel creation
- #8807 Facebook livechat integration
- #8149 Feature/livechat hide email
- #9009 Improve room types API and usages
- #8882 New Modal component
- #8029 Option to enable/disable auto away and configure timer
- #8866 Room counter sidebar preference
- #8979 Save room's last message
- #8905 Send category and title fields to iOS push notification
- #7999 Sender's name in email notifications.
- #8459 Setting to disable MarkDown and enable AutoLinker
- #8362 Sidebar item width to 100%
- #8360 Smaller accountBox
- #8060 Token Controlled Access channels
- #8361 Unify unread and mentions badge
- #8715 Upgrade Meteor to 1.6
- #8073 Upgrade to meteor 1.5.2
- #8433 Use enter separator rather than comma in highlight preferences + Auto refresh after change highlighted words
- #9092 Modal
- #9066 Make Custom oauth accept nested usernameField

[1.5.1]
* Update Rocket.Chat to 0.60.2

[1.5.2]
* Update Rocket.Chat to 0.60.3

[1.5.3]
* Update Rocket.Chat to 0.60.4
* #9343 LDAP TLS not working in some cases
* #9330 announcement hyperlink color
* #9335 Deleting message with store last message not removing
* #9345 last message cutting on bottom
* #9328 popover on safari for iOS
* #9364 Highlight setting not working correctly

[1.6.0]
* Update Rocket.Chat to 0.61.0
* #9048 Decouple livechat visitors from regular users
* #9366 add /home link to sidenav footer logo
* #9107 Add impersonate option for livechat triggers
* #9228 Add mention-here permission #7631
* #9053 Add support to external livechat queue service provider
* #8411 Contextual Bar Redesign
* #9234 Indicate the Self DM room
* #9135 Livechat extract lead data from message
* #9245 new layout for emojipicker
* #9216 Sidebar menu option to mark room as unread
* #9442 Update documentation: provide example for multiple basedn
* #9510 Contextual bar mail messages

[1.6.1]
* Update Rocket.Chat to 0.61.1
* #9714 Close Livechat conversation by visitor not working in version 0.61.0
* #9639 Desktop notification not showing when avatar came from external storage service
* #9640 Facebook integration in livechat not working on version 0.61.0
* #9067 Formal pronouns and some small mistakes in German texts
* #9716 GitLab OAuth does not work when GitLab’s URL ends with slash
* #9720 Messages can't be quoted sometimes
* #9626 Missing string 'There_are_no_applications' on the OAuth Apps Page
* #9623 Weird rendering of emojis at sidebar when last message is activated

[1.6.2]
* Update Rocket.Chat to 0.61.2
* #9776 Emoji rendering on last message
* #9772 Livechat conversation not receiving messages when start without form
* #9750 Livechat issues on external queue and lead capture

[1.7.0]
* Update Rocket.Chat to 0.62.0
* #9549 Add route to get user shield/badge
* #9457 Add user settings / preferences API endpoint
* #7098 Alert admins when user requires approval & alert users when the account is approved/activated/deactivated
* #9527 Allow configuration of SAML logout behavior
* #8193 Allow request avatar placeholders as PNG or JPG instead of SVG
* #9312 Allow sounds when conversation is focused
* #9519 API to fetch permissions & user roles
* #9642 Browse more channels / Directory
* #9778 General alert banner
* #9687 Global message search (beta: disabled by default)
* #8158 GraphQL API
* #9298 Improved default welcome message
* #8933 Internal hubot support for Direct Messages and Private Groups
* #9255 Livestream tab
* #9746 Makes shield icon configurable
* #9717 Message read receipts
* #9507 New REST API to mark channel as read
* #9608 New sidebar layout
* #9699 Option to proxy files and avatars through the server
* #9509 REST API to use Spotlight
* #9793 Version update check
* #9934 Typo on french translation for "Open"

[1.7.1]
* Update Rocket.Chat to 0.62.1
* [Release post](https://rocket.chat/2018/03/06/rocket-chat-0-62-released/)
* #9986 Delete user without username was removing direct rooms of all users
* #9960 Empty sidenav when sorting by activity and there is a subscription without room
* #9988 New channel page on medium size screens
* #9982 Two factor authentication modal was not showing

[1.7.2]
* Update Rocket.Chat to 0.62.2
* #10029 Download links was duplicating Sub Paths
* #10061 Message editing is crashing the server when read receipts are enabled
* #10009 REST API: Can't list all public channels when user has permission view-joined-room
* #10071 Slack Import reports invalid import file type due to a call to BSON.native() which is now doesn't exist
* #10076 Update preferences of users with settings: null was crashing the server
* #9719 Verified property of user is always set to false if not supplied

[1.8.0]
* Update Rocket.Chat to 0.63.0

[1.8.1]
* Update Rocket.Chat to 0.63.1
* Downgrade node to 8.9.4 to prevent segfaults
* [Complete changelog](https://forums.rocket.chat/t/rocket-chat-0-63-0-released-updated-for-0-63-1/479)
* Change deprecated Meteor._reload.reload method in favor of Reload._reload (#10348 by @tttt-conan)
* Add '.value' in the SAML package to fix TypeErrors on SAML token validation (#10084 by @TechyPeople)
* Incorrect german translation of user online status (#10356 by @kaiiiiiiiii)
* Incorrect French language usage for Disabled (#10355)

[1.8.2]
* Update Rocket.Chat to 0.63.2

[1.8.3]
* Update Rocket.Chat to 0.63.3

[1.9.0]
* Update Rocket.Chat to 0.64.0

[1.9.1]
* Update Rocket.Chat to 0.64.1
* Store the last sent message to show bellow the room's name by default (#10597)
* E-mails were hidden some information (#10615)
* Regression on 0.64.0 was freezing the application when posting some URLs (#10627)

[1.9.2]
* Update Rocket.Chat to 0.65.1

[1.10.0]
* Update Rocket.Chat to 0.65.2

[1.11.0]
* Update Rocket.Chat to 0.66.0

[1.11.1]
* Update Rocket.Chat to 0.66.1

[1.11.2]
* Update Rocket.Chat to 0.66.2

[1.11.3]
* Update Rocket.Chat to 0.66.3

[1.12.0]
* Update Rocket.Chat to 0.67.0
* sort fname sidenav (#11358)
* SVG icons code (#11319)
* Message popup responsiveness in slash commands (#11313)
* web app manifest errors as reported by Chrome DevTools (#9991 by @justinribeiro)
* Message attachment's fields with different sizes (#11342)
* Parse inline code without space before initial backtick (#9754 by @c0dzilla)

[1.13.0]
* Update Rocket.Chat to 0.68.0
* Set default max upload size to 100mb (#11327)
* Typing indicators now use Real Names (#11164 by @vynmera)
* Allow markdown in room topic, announcement, and description including single quotes (#11408)
* Livechat File Upload (#10514)

[1.13.1]
* Update Rocket.Chat to 0.68.2

[1.13.2]
* Update Rocket.Chat to 0.68.3

[1.13.3]
* Update Rocket.Chat to 0.68.4

[1.13.4]
* Update Rocket.Chat to 0.68.5

[1.14.0]
* Update Rocket.Chat to 0.69.1

[1.14.1]
* Update Rocket.Chat to 0.69.2

[1.15.0]
* Update Rocket.Chat to 0.70.0

[1.15.1]
* Update Rocket.Chat to 0.70.3

[1.16.0]
* Use latest base image
* Update Rocket.Chat to 0.70.4

[1.17.0]
* Update Rocket.Chat to 0.71.0

[1.17.1]
* Update Rocket.Chat to 0.71.1
* Email sending with GDPR user data (#12487)

[1.18.0]
* Update Rocket.Chat to 0.72.0
* Add permission to enable personal access token to specific roles (#12309)
* /api/v1/spotlight: return joinCodeRequired field for rooms (#12651)
* New API Endpoints for the new version of JS SDK (#12623)
* Setting to configure robots.txt content (#12547)
* Make Livechat's widget draggable (#12378)

[1.18.1]
* Update Rocket.Chat to 0.72.1

[1.18.2]
* Update Rocket.Chat to 0.72.2

[1.18.3]
* Update Rocket.Chat to 0.72.3

[1.19.0]
* Update Rocket.Chat to 0.73.1

[1.19.1]
* Update Rocket.Chat to 0.73.2

[1.20.0]
* Update Rocket.Chat to 0.74.0

[1.20.1]
* Update Rocket.Chat to 0.74.1

[1.20.2]
* Update Rocket.Chat to 0.74.2

[1.20.3]
* Update Rocket.Chat to 0.74.3

[1.21.0]
* Update Rocket.Chat to 1.1.3
* Update manifest to v2
* Add mongodb oplog tailing

[1.22.0]
* Use username as the unique id for portability

[1.23.0]
* Update Rocket.Chat to 1.2.1

[1.23.1]
* Update Rocket.Chat to 1.2.3

[1.24.0]
* Update Rocket.Chat to 1.3.0

[1.24.1]
* Update Rocket.Chat to 1.3.1

[1.24.2]
* Update Rocket.Chat to 1.3.2

[1.25.0]
* Update Rocket.Chat to 2.0.0

[1.26.0]
* Update Rocket.Chat to 2.1.0

[1.26.1]
* Update Rocket.Chat to 2.1.1
* Read Recepts was not working (#15603)
* Dynamic import of JS files were not working correctly (#15598)

[1.26.2]
* Update Rocket.Chat to 2.1.2
* Read Receipts were not working properly with subscriptions without ls (#15656)
* Exception when sending email of messages attachments undefined (#15657)
* Channel Announcements not working (#14635 by @knrt10)

[1.27.0]
* Update Rocket.Chat to 2.2.0
* Full changelog https://github.com/RocketChat/Rocket.Chat/releases/tag/2.2.0

[1.27.1]
* Update Rocket.Chat to 2.2.1

[1.28.0]
* Update Rocket.Chat to 2.3.0

[1.28.1]
* Update Rocket.Chat to 2.3.1
* Default value of the Livechat WebhookUrl setting (#15898)
* Admin menu not showing after renamed integration permissions (#15937 by @n-se)
* Administration UI issues (#15934)
* Livechat permissions being overwrite on server restart (#15915)
* Livechat triggers not firing (#15897)
* Auto load image user preference (#15895)

[1.28.2]
* Update Rocket.Chat to 2.3.2

[1.29.0]
* Update Rocket.Chat to 2.4.0

[1.29.1]
* Update Rocket.Chat to 2.4.1
* Enable apps change properties of the sender on the message as before (#16189)
* Add missing password field back to administration area (#16171)
* JS errors on Administration page (#16139)
* Full changelog https://github.com/RocketChat/Rocket.Chat/releases/tag/2.4.1

[1.29.2]
* Update Rocket.Chat to 2.4.2

[1.29.3]
* Update Rocket.Chat to 2.4.5

[1.29.4]
* Update Rocket.Chat to 2.4.6

[1.29.5]
* Update Rocket.Chat to 2.4.7

[1.29.6]
* Update Rocket.Chat to 2.4.8

[1.29.7]
* Update Rocket.Chat to 2.4.9

[2.0.0]
* WARNING: Rocket.Chat now requires at least 2GB memory! You may have to adjust the memory limit.
* Update Rocket.Chat to 3.0.2
* Filter System messages per room (#16369)
* Remove deprecated publications (#16351)
* Hide system messages (#16243)
* Upgrade to Meteor 1.9 and NodeJS 12 (#16252)
* Removed room counter from sidebar (#16036)
* Change apps/icon endpoint to return app's icon and use it to show on Ui Kit modal (#16522)
* Button to download admin server info (#16059)
* UiKit - Interactive UI elements for Rocket.Chat Apps (#16048)
* Sort the Omnichannel Chat list according to the user preferences (#16437)
* Setting to only send plain text emails (#16065)
* Check the Omnichannel service status per Department (#16425)
* Create a user for the Apps during installation (#15896 by @Cool-fire)
* Add GUI for customFields in Omnichannel conversations (#15840 by @antkaz)
* update on mongo, node and caddy on snap (#16167)
* Enforce plain text emails converting from HTML when no text version supplied (#16063)
* Setting Top navbar in embedded mode (#16064)
* Full [changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.0.0)

[2.0.1]
* Update Rocket.Chat to 3.0.3
* Language country has been ignored on translation load (#16757)
* Manual Register use correct state for determining registered (#16726)
* Rocket.Chat takes too long to set the username when it fails to send enrollment email (#16723)
* LDAP sync admin action was not syncing existent users (#16671)
* Check agent status when starting a new conversation with an agent assigned (#16618)

[2.0.2]
* Update Rocket.Chat to 3.0.4

[2.0.3]
* Update Rocket.Chat to 3.0.5
* Fix race conditions on/before login (#16989)

[2.0.4]
* Update Rocket.Chat to 3.0.7
* Keeps the agent in the room after accepting a new Omnichannel request (#16787)
* Regression: Remove deprecated Omnichannel setting used to fetch the queue data through subscription (#17017)

[2.0.6]
* Update Rocket.Chat to 3.0.10
* Apps Engine: Reduce some stream calls and remove a find user from the app's status changes (#17115)
* Federation Event ROOM_ADD_USER not being dispatched (#16878 by @1rV1N-git)
* Federation delete room event not being dispatched (#16861 by @1rV1N-git)

[2.0.7]
* Update Rocket.Chat to 3.0.12
* Fix: Missing checks for Troubleshoot > Disable Notifications (#17155)
* Fix: Error message on startup of multiple instances related to the metrics’ server (#17152)

[2.1.0]
* Update Rocket.Chat to 3.1.1

[2.1.1]
* Include customized serbian live-chat translation

[2.2.0]
* Update Rocket.Chat to 3.2.0
* Use latest base image 2.0.0

[2.2.1]
* Update Rocket.Chat to 3.2.1

[2.2.2]
* Update Rocket.Chat to 3.2.2
* Install mongo-tools for mongorestore

[2.3.0]
* Update Rocket.Chat to 3.3.0

[2.3.1]
* Update Rocket.Chat to 3.3.3

[2.4.0]
* Update Rocket.Chat to 2.4.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.4.0)
* Update manifest tags, screenshots and forum url

[2.4.1]
* Update Rocket.Chat to 2.4.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.4.1)

[2.4.2]
* Update Rocket.Chat to 2.4.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.4.2)

[2.5.0]
* Update Rocket.Chat to 3.5.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.5.0)
* Most notably the Push Notification Gateway changed and will raise a notification banner upon update for RocketChat admins.

[2.5.1]
* Update Rocket.Chat to 3.5.1
* Migration 194 (#18457 by @thirsch)
* Omnichannel session monitor is not starting (#18412)
* Invalid MIME type when uploading audio files (#18426)
* Multiple push notifications sent via native drivers (#18442)
* Appending 'false' to Jitsi URL (#18430)
* Can't send long messages as attachment (#18355)

[2.5.2]
* Update Rocket.Chat to 3.5.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.5.2)
* Sending notifications from senders without a name (#18479)

[2.5.3]
* Update Rocket.Chat to 3.5.3
* UIKit Select and Multiselects not working (#18598)
* React being loaded on the main bundle (#18597)
* Users page in admin not working for inactive user joining (#18594)

[2.5.4]
* Update Rocket.Chat to 3.5.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.5.4)

[2.6.0]
* Update Rocket.Chat to 3.6.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.6.0)
* Jitsi: Setting to use room's name instead of room's id to generate the URL (#17481)
* Omnichannel: Ability to set character message limit on Livechat widget (#18261 by @oguhpereira)
* Omnichannel: Livechat widget support for rich messages via UiKit (#18643)
* Omnichannel/API: Endpoint livechat/room.visitor to change Omnichannel room's visitor (#18528)
* Omnichannel/API: Endpoint livechat/visitors.search to search Livechat visitors (#18514)
* Admin option to reset other users’ E2E encryption key (#18642)
* Requires the 2FA password fallback enforcement enabled to work
* Banner for servers in the middle of the cloud registration process (#18623)
* Export room messages as file or directly via email (#18606)
* Support for custom avatar images in channels (#18443)

[2.6.1]
* Update Rocket.Chat to 3.6.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.6.1)
* File upload (Avatars, Emoji, Sounds) (#18841)
* IE11 support livechat widget (#18850)
* Omnichannel Current Chats open status filter not working (#18795)
* Showing alerts during setup wizard (#18862)
* User administration throwing a blank page if user has no role (#18851)
* User can't invite or join other Omnichannel rooms (#18852)

[2.6.2]
* Update Rocket.Chat to 3.6.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.6.2)
* Create Custom OAuth services from environment variables (#17377 by @mrtndwrd)
* Deactivate users that are the last owner of a room using REST API (#18864)
* Allow for user deactivation through REST API (even if user is the last owner of a room)
* Ignore User action from user card (#18866)
* invite-all-from and invite-all-to commands don't work with multibyte room names (#18919)

[2.6.3]
* Update Rocket.Chat to 3.6.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.6.3)
* Errors in LDAP avatar sync preventing login (#18948)
* Federation issues (#18978)
* LDAP avatar upload (#18994)
* PDF not rendering (#18956)

[2.7.0]
* Update Rocket.Chat to 3.7.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.7.0)

[2.7.1]
* Update Rocket.Chat to 3.7.1
* Adding missing custom fields translation in my account's profile (#19179)
* Admin Sidebar overflowing (#19101)
* Missing "Bio" in user's profile view (#18821) (#19166)
* Performance issues when using new Oplog implementation (#19181)
* A missing configuration was not limiting the new oplog tailing to pool the database frequently even when no data was available, leading to both node and mongodb process been consuming high CPU even with low usage. This case was happening for installations using mmapv1 database engine or when no admin access was granted to the database user, both preventing the usage of the new Change Streams implementation and fallbacking to our custom oplog implementation in replacement to the Meteor's one what was able to be disabled and use the native implementation via the environmental variable USE_NATIVE_OPLOG=true.
* VisitorAutoComplete component (#19133)

[2.8.0]
* Update Rocket.Chat to 3.8.0
* Update Node.js to 12.18.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.8.0)

[2.8.1]
* Update Rocket.Chat to 3.8.1
* IE11 - Update ui kit and fuselage bundle (#19561)
* Typo in custom oauth from environment variable (#19570)
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.8.1)

[2.8.2]
* Update Rocket.Chat to 3.8.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.8.2)
* Room avatar update event doesn't properly broadcast room id (#19684)
* Server crash while reading settings for allowed and blocked email domain lists (#19683)

[2.9.0]
* Update Rocket.Chat to 3.9.1
* [Full changelog 3.9.0](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.9.0)
* [Full changelog 3.9.1](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.9.1)

[2.10.0]
* Enable turn addon

[2.10.1]
* Update Rocket.Chat to 3.9.2
* [Full changelog 3.9.1](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.9.2)
* 'Not Allowed' in message auditing (#19762)
* Download my data with file uploads (#19862)
* Forgot password endpoint return status (#19842)

[2.10.2]
* Update Rocket.Chat to 3.9.3
* Issue with special message rendering (#19817)
* Problem with attachment render (#19854)

[2.11.0]
* Update Rocket.Chat to 3.10.0
* [Full changelog 3.9.1](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.10.0)
* Custom scroll (#19701)
* Omnichannel Contact Center (Directory) (#19931)
* REST Endpoint instances.get (#19926)
* Returns an array of instances on the cluster.
* REST endpoints to add and retrieve Enterprise licenses (#19925)
* Update Checker Description (#19892)
* User preference for audio notifications (#19924)

[2.11.1]
* Update Rocket.Chat to 3.10.3
* Fix ser registration updating wrong subscriptions (#20128)
* Fix tabbar is opened (#20122)
* Users can be removed from channels without any error message.
* Agent information panel not rendering (#19965)
* Creation of Omnichannel rooms not working correctly through the Apps when the agent parameter is set (#19997)
* Messages being updated when not required after user changes his profile (#20114)
* OAuth users being asked to change password on second login (#20003)
* Omnichannel Agents unable to take new chats in the queue (#20022)
* Omnichannel Business Hours form is not being rendered (#20007)
* Omnichannel raw model importing meteor dependency (#20093)
* Omnichannel rooms breaking after return to queue or forward (#20089)
* User Audio notification preference not being applied (#20061)

[2.11.2]
* Update Rocket.Chat to 3.10.4
* Room's list showing all rooms with same name (#20176)
* Change console.warning() to console.warn() (#20200)

[2.11.3]
* Update Rocket.Chat to 3.10.5
* Security Hotfix

[2.12.0]
* Update Rocket.Chat to 3.11.0
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.11.0)

[2.13.0]
* Update Rocket.Chat to 3.11.1
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.11.1)
* Use base image v3
* Attachment download from title fixed (#20585 by @yash-rajpal)
* Added target = '_self' to attachment link, this seems to fix the problem, without this attribute, error page is displayed.
* Gif images aspect ratio on preview (#20654)

[2.13.1]
* Add a way to set custom env vars via `/app/data/env`

[2.14.0]
* Update Rocket.Chat to 3.12.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.12.0)

[2.14.1]
* Update Rocket.Chat to 3.12.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.12.1)

[2.14.2]
* Update Rocket.Chat to 3.12.3
* Bump Livechat widget
* Security Hotfix

[2.15.0]
* Update Rocket.Chat to 3.13.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.13.0)
* Add spacing between elements in Profile Page (#20742 by @cyberShaw)
* Added modal-box for preview after recording audio. (#20370 by @Darshilp326)
* Better new channel popover (#21018)
* Improve Apps permission modal (#21193)
* Re-design Omnichannel Room Info Panel (#21199)
* Set description in create channel modal (#21132)
* Sort Users List In Case Insensitive Manner (#20790 by @aditya-mitra)

[2.15.1]
* Update Rocket.Chat to 3.13.1
* Update nodejs to 12.21.0
* Add tag input to Closing Chat modal (#21462)
* Admin Users list pagination (#21469)
* Fix Administration/Users pagination
* App installation from marketplace not correctly displaying the permissions (#21470)
* Fixes the marketplace app installation not correctly displaying the permissions modal.
* Close chat button is not available for Omnichannel agents (#21481)
* Error when editing Omnichannel rooms without custom fields (#21450)
* Header component breaking if user is not part of teams room. (#21465)
* Make Omnichannel's closing chat button the last action in the toolbox (#21476)
* Omnichannel queue manager returning outdated room object (#21485)
* The Omnichannel Queue Manager is returning outdated room object when delegating the chat to an agent, hence, our Livechat widget is affected and the agent assigned to the chat is not displayed on the widget, only after refreshing/reloading.
* Wrong useMemo on Priorities EE field. (#21453)

[2.15.2]
* Update Rocket.Chat to 3.13.2
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.15.3]
* Update Rocket.Chat to 3.13.3
* Livechat not retrieving messages (#21644 by @cuonghuunguyen)
* Team's channels list for teams with too many channels (#21491)

[2.15.4]
* Update Rocket.Chat to 3.14.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.14.0)

[2.15.5]
* Update Rocket.Chat to 3.14.1
* Paginated and Filtered selects on new/edit unit (#22052)
* Forwarding Department behaviour with Waiting queue feature (#22043)
* Omnichannel Room Information panel flow when user save or close on form page. (#21688)

[2.15.6]
* Update Rocket.Chat to 3.14.2
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.15.7]
* Update Rocket.Chat to 3.14.4
* Discussion names showing a random value (#22172)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.16.0]
* Update Rocket.Chat to 3.15.0
* Ability for Rocket.Chat Apps to delete rooms (#21875)
* Adds a new delete method on the rooms bridge in order to trigger the deletion of rooms via the Apps-Engine.

[2.16.1]
* Update Rocket.Chat to 3.15.1
* Attachments and avatars not rendered if deployed on subfolder (#22290)
* Setup wizard infinite loop when on subfolder. (#22395)
* Support DISABLE_PRESENCE_MONITOR env var in new DB watchers (#22257)

[2.16.2]
* Update Rocket.Chat to 3.15.2
* Sound notification is not emitted when the Omnichannel chat comes from another department (#22291)
* Visitor info screen being updated multiple times (#22482)

[2.17.0]
* Update Rocket.Chat to 3.16.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.16.0)

[2.17.1]
* Update Rocket.Chat to 3.16.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.16.1)
* Livechat apps permission error (#22511)
* Prune messages not applying the user filter (#22506)

[2.17.2]
* Update Rocket.Chat to 3.16.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.16.2)
* Checks the list of agents if at least one is online (#22584)
* Error in permission check for getLivechatDepartmentByNameOrId method in Apps (#22545)
* Update the Apps-Engine with a fix for the permission check on the getLivechatDepartmentByNameOrId method
* Livechat webhook request without headers (#22589)
* Markdown for UiKit blocks (#22619)

[2.17.3]
* Update Rocket.Chat to 3.16.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.16.3)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.17.4]
* Update Rocket.Chat to 3.16.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.16.4)
* Fix Content-Security-Policy ignoring CDN configuration (#22791 by @nmagedman)

[2.18.0]
* Update Rocket.Chat to 3.17.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.17.0)
* roles.delete endpoint (#22497 by @lucassartor)
* Collect data about LDAP, SAML, CAS and OAuth usage. (#22719)
* Convert Team to Channel (#22476)
* Federation setup (#22208)
* Logout other user endpoint (#22661)

[2.18.1]
* Update Rocket.Chat to 3.17.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.17.1)

[2.18.2]
* Update Rocket.Chat to 3.17.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.17.2)
* applyChatRestictions callback not working for community version (#22839 by @Shailesh351)
* Error getting default agent when routing system algorithm is Auto Selection (#22976)
* Fix Auto Selection algorithm on community edition (#22991)

[2.19.0]
* Update Rocket.Chat to 3.18.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.18.0)

[2.19.1]
* Update Rocket.Chat to 3.18.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.18.1)
* Change HTTP and Method logs to level INFO (#23100)
* Change way emails are validated on livechat registerGuest method (#23089)

[2.19.2]
* Update Rocket.Chat to 3.18.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/3.18.2)
* [Security Hotfix](https://docs.rocket.chat/guides/security/security-updates)
* Update visitor info on email reception based on current inbox settings (#23280)

[2.20.0]
* Update Rocket.Chat to 4.0.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.0.0)
* APPS: Get livechat's room transcript via bridge method (#22985)
* Adds a new method for retrieving a room's transcript via a new method in the Livechat bridge
* Omnichannel source identification fields (#23090)
* Seats Cap (#23017 by @g-thome)

[2.20.1]
* Update Rocket.Chat to 4.0.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.0.1)
* Fixes BigBlueButton integration
* imported migration v240 (#23374)
* LDAP password not checked (#23382)
* resumeToken not working (#23379)

[2.20.2]
* Update Rocket.Chat to 4.0.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.0.3)
* Attachment buttons overlap in mobile view (#23377 by @Aman-Maheshwari)
* Prevent starting Omni-Queue if Omnichannel is disabled (#23396)
* Fix user/agent upload not working via Apps Engine after 3.16.0 (#23393)
* Communication problem when updating and uninstalling apps in cluster (#23418)
* Server crashing when Routing method is not available at start (#23473)

[2.20.3]
* Update Rocket.Chat to 4.0.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.0.4)
* Queue error handling and unlocking behavior (#23522)

[2.21.0]
* Pre-setup admin user for new installations
* Disable registration by default on new installations

[2.21.1]
* Update Rocket.Chat to 4.0.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.0.5)
* OAuth login not working on mobile app

[2.22.0]
* Update Rocket.Chat to 4.1.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.1.0)
* Stream to get individual presence updates (#22950)
* Add markdown to custom fields in user Info (#20947 by @yash-rajpal)
* Added markdown to custom fields to render links
* Allow Omnichannel to handle huge queues (#23392)
* Make Livechat Instructions setting multi-line (#23515)

[2.22.1]
* Update Rocket.Chat to 4.1.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.1.1)
* Advanced LDAP Sync Features (#23608)
* App update flow failing in HA setups (#23607)
* LDAP users not being re-activated on login (#23627)

[2.22.2]
* Update Rocket.Chat to 4.1.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.1.2)
* Notifications are not being filtered (#23487)
* Add a migration to update the Accounts_Default_User_Preferences_pushNotifications setting's value to the Accounts_Default_User_Preferences_mobileNotifications setting's value;
* Remove the Accounts_Default_User_Preferences_mobileNotifications setting (replaced by Accounts_Default_User_Preferences_pushNotifications);
* Rename 'mobileNotifications' user's preference to 'pushNotifications'.
* Omnichannel status being changed on page refresh (#23587)
* Performance issues when running Omnichannel job queue dispatcher (#23661)

[2.23.0]
* Update Rocket.Chat to 4.2.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.2.0)
* Allow Omnichannel statistics to be collected. (#23694)
* This PR adds the possibility for business stakeholders to see what is actually being used of the Omnichannel integrations.
* Allow registering by REG_TOKEN environment variable (#23737)
* You can provide the REG_TOKEN environment variable containing a registration token and it will automatically register to your cloud account. This simplifies the registration flow
* Audio and Video calling in Livechat (#23004 by @Deepak-learner & @dhruvjain99)
* Enable LDAP manual sync to deployments without EE license (#23761)
* Open the Enterprise LDAP API that executes background sync to be used without any Enterprise License and enforce 2FA requirements.
* Permission for download/uploading files on mobile (#23686)
* Permissions for interacting with Omnichannel Contact Center (#23389)
* Adds a new permission, one that allows for control over user access to Omnichannel Contact Center,
* Rate limiting for user registering (#23732)
* REST endpoints to manage Omnichannel Business Units (#23750)
* Basic documentation about endpoints can be found at https://www.postman.com/kaleman960/workspace/rocketchat-public-api/request/3865466-71502450-8c8f-42b4-8954-1cd3d01fcb0c

[2.23.1]
* Update Rocket.Chat to 4.2.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.2.1)
* Create new setting to clean local storage at end of chats (#23821)
* Error when creating an inactive user in admin panel (#23859)

[2.23.2]
* Update Rocket.Chat to 4.2.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.2.2)
* creating room with federated member (#23347 by @qwertiko)

[2.24.0]
* Update Rocket.Chat to 4.3.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.3.0)
* Update base image to 3.2.0

[2.24.1]
* Update Rocket.Chat to 4.3.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.3.1)

[2.24.2]
* Update Rocket.Chat to 4.3.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.3.2)
* App Framework Enable hanging indefinitely (#24158)
* CSV Importer failing to import users (#24090)
* Integration section crashing opening in My Account (#24068)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.24.3]
* Update Rocket.Chat to 4.3.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.3.3)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.25.0]
* Update Rocket.Chat to 4.4.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.4.0)
* Update nodejs to 14.18.2
* App empty states component, category filter and empty states error variation implementations (#23818)

[2.25.1]
* Update Rocket.Chat to 4.4.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.4.1)
* Add ?close to OAuth callback url (#24381)
* Oembed request not respecting payload limit (#24418)
* Outgoing webhook without scripts not saving messages (#24401)
* Skip cloud steps for registered servers on setup wizard (#24407)
* Slash commands previews not working (#24387)
* Startup errors creating indexes (#24409)
* Fix bio and prid startup index creation errors.

[2.25.2]
* Update Rocket.Chat to 4.4.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.4.2)
* OAuth mismatch redirect_uri error (#24450)

[2.26.0]
* Update Rocket.Chat to 4.5.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.5.0)
* E2E password generator (#24114)
* Marketplace sort filter (#24567)
* VoIP Support for Omnichannel (#23102)

[2.26.1]
* Update Rocket.Chat to 4.5.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.5.1)
* Missing username on messages imported from Slack (#24674)
* no id of room closer in livechat-close message (#24683)
* Reload roomslist after successful deletion of a room from admin panel. (#23795 by @Aman-Maheshwari)
* Also added a succes toast message after the successful deletion of room.
* Room's message count not being incremented on import (#24696)
* Show only available agents on extension association modal (#24680)
* System messages are sent when adding or removing a group from a team (#24743)

[2.26.2]
* Update Rocket.Chat to 4.5.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.5.2)
* Voip Extensions disabled state (#24750)
* "livechat/webrtc.call" endpoint not working (#24804)
* PaginatedSelectFiltered not handling changes (#24732)
* Broken multiple OAuth integrations (#24705)
* Critical: Incorrect visitor getting assigned to a chat from apps (#24805)

[2.26.3]
* Update Rocket.Chat to 4.5.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.5.3)
* Standarize queue behavior for managers and agents when subscribing (#24837)
* UX - VoIP Call Component (#24748)

[2.26.4]
* Update Rocket.Chat to 4.5.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.5.4)

[2.26.5]
* Update Rocket.Chat to 4.5.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.5.5)
* High CPU usage caused by CallProvider (#24994)
* Multiple issues starting a new DM (#24955)

[2.27.0]
* Update Rocket.Chat to 4.6.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.6.0)
* Telemetry Events (#24781 by @eduardofcabrera)

[2.27.1]
* Update Rocket.Chat to 4.6.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.6.1)
* FormData uploads not working (#25069)
* Invitation links don't redirect to the registration form (#25082)
* NPS never finishing sending results (#25067)
* Proxy settings being ignored (#25022)
* Modify Meteor's HTTP.call to add back proxy support
* Upgrade Tab showing for a split second (#25050)
* UserAutoComplete not rendering UserAvatar correctly (#25055)

[2.27.2]
* Update Rocket.Chat to 4.6.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.6.2)
* Database indexes not being created (#25101)
* Deactivating user breaks if user is the only room owner (#24933 by @sidmohanty11)

[2.27.3]
* Update Rocket.Chat to 4.6.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.6.3)
* Desktop notification on multi-instance environments (#25220)

[2.28.0]
* Update Rocket.Chat to 4.7.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.7.0)
* Add expire index to integration history (#25087)
* Alpha Matrix Federation (#23688)
* Expand Apps Engine's environment variable allowed list (#23870 by @cuonghuunguyen)
* Message Template React Component (#23971)
* Add OTR Room States (#24565)
* Add tooltip to sidebar room menu (#24405 by @Himanshu664)
* Added MaxNickNameLength and MaxBioLength constants (#25231 by @aakash-gitdev)
* Added tooltip options for message menu (#24431 by @Himanshu664)
* Improve active/hover colors in account sidebar (#25024 by @Himanshu664)

[2.28.1]
* Update Rocket.Chat to 4.7.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.7.1)
* Use setting to determine if initial general channel is needed (#25441)
* LDAP sync removing users from channels when multiple groups are mapped to it (#25434)
* Spotlight results showing usernames instead of real names (#25471)

[2.28.2]
* Update Rocket.Chat to 4.7.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.7.2)
* Dynamic load matrix is enabled and handle failure (#25495)
* Initial User not added to default channel (#25544)
* If injecting initial user. The user wasn’t added to the default General channel
* One of the triggers was not working correctly (#25409)
* UI/UX issues on Live Chat widget (#25407)
* User abandonment setting is working again (#25520)

[2.28.3]
* Update Rocket.Chat to 4.7.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.7.4)
* Security Hotfix

[2.29.0]
* Update Rocket.Chat to 4.8.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.8.0)
* Ability for RC server to check the business hour for a specific department (#25436)
* Add expire index to integration history (#25087)
* Add new app events for pin, react and follow message (#25337)
* Add new events after user login, logout and change his status (#25234)
* Add option to show mentions badge when show counter is disabled (#25329)
* Add user events for apps (#25165)
* Adding app button on user dropdown (#25326)
* Alpha Matrix Federation (#23688)

[2.29.1]
* Update Rocket.Chat to 4.8.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.8.1)
* AccountBox checks for condition (#25708)
* Bump meteor-node-stubs to version 1.2.3 (#25669 by @Sh0uld)
* With meteor-node-stubs version 1.2.3 a bug was fixed, which occured in issue #25460 and probably #25513 (last one not tested).
* For the issue in meteor see: meteor/meteor#11974
* Fix prom-client new promise usage (#25781)
* Wrong argument name preventing Omnichannel Chat Forward to User (#25723)

[2.29.2]
* Update Rocket.Chat to 4.8.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/4.8.2)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)
* Error "numRequestsAllowed" property in rateLimiter for REST API endpoint when upgrading (#26058)
* Not showing edit message button when blocking edit after N minutes (#25724 by @matthias4217)
* Previously, in Rocketchat 4.7.0 and later, as mentioned in https://github.com/RocketChat/Rocket.Chat/issues/25478, the edit button was not displayed on the interface in the minute after having sent a message. This is now fixed : messages can be edited right after sending them.
* Settings not being overwritten to their default values (#25891)

[2.30.0]
* Update Rocket.Chat to 5.0.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.0.0)
* Update nodejs to 14.19.3

[2.30.1]
* Update Rocket.Chat to 5.0.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.0.1)
* Use single change stream to watch DB changes
* Not possible to deactivate users

[2.31.0]
* Email display name support

[2.31.1]
* Update Rocket.Chat to 5.0.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.0.2)
* Empty results on im.list endpoint (#26438)
* Undefined MediaDevices error on HTTP (#26396)

[2.31.2]
* Update Rocket.Chat to 5.0.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.0.3)
* Chats holds to load history for some time (#26425)
* Endpoints not working when using "Use Real Name" setting (#26530 by @kodiakhq[bot])

[2.31.3]
* Update Rocket.Chat to 5.0.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.0.4)

[2.31.4]
* Update Rocket.Chat to 5.0.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.0.5)
* Business Units endpoints not filtering by Unit type (#26713)
* Omnichannel inquiries being updated even if not needed (#26692)

[2.32.0]
* Update Rocket.Chat to 5.1.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.1.0)
* Adding oauth crud on the rocket.chat side (#26220 by @kodiakhq[bot])
* allow ephemeral messages to receive a specific id (#26118 by @kodiakhq[bot])
* Capability to search visitors by custom fields (#26312)
* Fallback Error component for Engagement Dashboard widgets (#26441 by @kodiakhq[bot])
* Marketplace apps page new list view layout (#26181)
* Surface featured apps endpoint (#26416)
* Added identification on calls to/from existing contacts (#26334)
* General federation improvements (#26150)
* New 'not found page' design (#26452 by @kodiakhq[bot])
* OTR refactoring (#24757)
* Remove device-management banner and modal (#26729)
* Spotlight search user results (#26599 by @kodiakhq[bot])
* use enter key to call using DialPad (#26454)

[2.32.1]
* Update Rocket.Chat to 5.1.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.1.1)
* Fix broken legacy message view (#26819)
* Fixed messagesHistory function, it was filtering messages only with existing threads.
* Livechat trigger messages covering all the website (#26776)
* Restore current chats default table order (#26808)

[2.32.2]
* Update Rocket.Chat to 5.1.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.1.2)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.32.3]
* Update Rocket.Chat to 5.1.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.1.3)
* MongoInvalidArgumentError on overwriting existing setting (#26880)
* Check if messsage.replies exist on new message template (#26652)
* Error when mentioning a non-member of a public channel (#26917)
* Importer fails when file includes user without an email. (#26836)

[2.32.4]
* Update Rocket.Chat to 5.1.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.1.4)
* Adjusted livechat fallbacks to take null values into account (#26909)

[2.33.0]
* Update Rocket.Chat to 5.2.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.2.0)
* Move administration links to an exclusive kebab menu (#26867)
* Add Markup to QuoteAttachment (#26751)
* Get moderators, owners and leaders from room scope via apps-engine (#26674)
* Matrix Federation events coverage expansion (support for 5 more events) (#26705)
* Sections layout and featured apps for marketplace (#26514)
* Allow delete attachment description on message edit (#26673)
* Better descriptions for VoIP Settings (#26877)
* Changed dial pad appearance to match original design (#26863)
* Include syncAvatars on ldap.syncNow (#26824)
* OTR Message (#24297)
* Results of user auto complete (#26687)
* Rounded video attachment (#26832)
* Setting for login email notifications (#26831)
* System messages' consistency (#26130)
* Updating voip tooltips and icons (#26834)
* VideoConference Messages UI (#26548)
* VideoConference Messages UI (#26548)" (#26961)

[2.34.0]
* Update Rocket.Chat to 5.3.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.3.0)
* Add new endpoint 'livechat/room.saveInfo' & deprecate 'livechat:saveInfo' meteor method (#26789)
* Health check for data stream (#27026)
* Matrix federation events coverage expansion (adding support for 3 extra events) (#26859)
* Automatically open call info contextual bar when voip room is opened (#26963)
* Better /health response when service not healthy (#27091)

[2.34.1]
* Update Rocket.Chat to 5.3.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.3.1)
* File upload receiving whole file to apply limits (#27105)
* Hide system messages setting not being respected. (#27151)
* There was a query missing the parameters in the client.
* Also added a few tests to help reduce the risk of this happening again.
* Multi instance error message (#27243)
* Next schedule check for Apps (#27240)
* User merge by e-mail on OAuth is case-sensitive (#27167)

[2.34.2]
* Update Rocket.Chat to 5.3.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.3.2)
* Replace regex not compatible with safari (#27294)
* Sidebar Room list extended preview not updating on new messages. (#27259)

[2.34.3]
* Update Rocket.Chat to 5.3.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.3.3)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.34.4]
* Update Rocket.Chat to 5.3.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.3.4)
* Thread messages being included in the room history even though they are not displayed (#27391)

[2.34.5]
* Update Rocket.Chat to 5.3.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.3.5)
* Fix watch db being started on Micro Services env (#27435)

[2.35.0]
* Update Rocket.Chat to 5.4.0
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.4.0)
* Add new endpoint 'livechat/agent.status' & deprecate changeLivechatStatus meteor method (#27047)
* Bugsnag client (#23580)
* Custom MessageType for video conference messages (#27333)
* Emphasis Elements (italic, strike and bold) in Message Parser Components (#27003)
* Currently the message parser does not accept Emphasis elements mixed with plain text
* [Normal Link - *Bold*, _Italic_ and ~strike~ Styles](https://rocket.chat/) return plain text → Should return a Lin with bold/italic/strike
* To fix this behavior it was necessary to edit LinkSpan component in gazzodown package (changes included in this PR) and the grammar/utils files in the fuselage/message-parser repository
* Fuselage Pull Request: RocketChat/fuselage#887
* Federation events coverage expansion (#27119)
* REST API endpoint /v1/oauth-apps.create (#27054)
* Created the 'oauth-apps.addOAuthApp' endpoint for the apps/meteor/client/views/admin/oauthApps/OAuthAddApp.tsx file, and added Ajv validation.
* REST API endpoint /v1/rooms.delete (#26866)
* Video Conference Message blocks and info action (#27310)

[2.35.1]
* Update Rocket.Chat to 5.4.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.4.1)
* Custom languages not being applied to i18next (#27557)
* Fix Login with Show default form disabled (#27475)
* Message Actions menu does not close upon choosing an action (#27328)
* Pagination not working on current chats (#27432)
* Registration and Login placeholders not being used (#27558)

[2.35.2]
* Update Rocket.Chat to 5.4.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.4.2)
* Emoji picker for large amount of custom emojis (#27745)
* `*.files` endpoints returning hidden files (#27617)
* New users aren't mapped to rooms with OAuth groups/channels map (#27000 by @carlosrodrigues94)
* This change deals with the problem with the new users coming from key cloak, this users were not being mapped to the correct channels on RC.
* Not all messages in chat conversations are available when forwarding between agents or departments. (#27816)
* OmnichannelRoomIconProvider order breaking VideoConfPopup (#27740)

[2.35.3]
* Update Rocket.Chat to 5.4.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.4.3)
* Security Hotfix (https://docs.rocket.chat/guides/security/security-updates)

[2.35.4]
* Update Rocket.Chat to 5.4.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/5.4.4)
* Chore: New temporary settings to limit access to files and outside room members (#28319)

[2.36.0]
* Update Rocket.Chat to 6.0.0
* Check the [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.0.0) for all the new features, bugfixes and also breaking changes

[2.36.1]
* Disable mongodb deprecation warning check

[2.36.2]
* Update Rocket.Chat to 6.0.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.0.1)
* fix: Apps.engine resource consumption (#28514)
* fix: Changing the app's error verification (#28450)
* fix: Horizontal scroll in main room if text is too long (#28434)
* fix: Offline register option (#28467)

[2.37.0]
* Update Rocket.Chat to 6.1.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.0)
* VideoConference Guest mode and Conference Router (#28186)
* Adding input type=password to AWS secrets fields (#28159)
* Some fields that store secrets for AWS were exposing sensitive data because they didn't have the input type as password.
* The change includes adding the password type and also changing the autocomplete value from <PasswordInput /> component to add the value new-password to avoid the browser autocompleting with the existing password. (This does not prevent the browser to show the hint to add the password)
* Engagement dashboard crash on Users tab (#28131)
* Gallery opening other image after closing (#27957)
* hide offline registration option for disconnected workspaces (#28662)
* Livechat MessageList not auto scrolling on new message (#28547)

[2.37.1]
* Update Rocket.Chat to 6.1.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.1)
* Messages jumping after reaction (#28770)

[2.37.2]
* Update Rocket.Chat to 6.1.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.2)
* Add function to get installation source (#28806)
* Fixed no role assign to manual registered users despite of Default Roles for Users settings (#28293 by @bhardwajdisha)
* Quotes chain off by one error in quote chain limit settings (#28281 by @jayesh-jain252)
* Fixed off by one error in Quote Chain Limit. Now if a user sets Message_QuoteChainLimit (default is 2) the number of quotes chained matches the setting.
* SMTP warnings on user form (#28832)
* Check for SMTP config on server side and then show warning on client side

[2.37.3]
* Update Rocket.Chat to 6.1.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.3)
* Livechat notifications not working correctly

[2.37.4]
* Update Rocket.Chat to 6.1.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.4)

[2.37.5]
* Update Rocket.Chat to 6.1.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.5)

[2.37.6]
* Update Rocket.Chat to 6.1.6
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.6)
* Livechat Triggers not working

[2.37.7]
* Update Rocket.Chat to 6.1.7
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.1.7)
* Migration error when removing 'snipetted' index

[2.38.0]
* Update Rocket.Chat to 6.2.8
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.2.0)
* add GA4 support (#28830 by @avelino)
* auto link custom domain (#28501)
* Custom roles and Public&Private apps stats (#27781)
* Make the marketplace search bar placeholder dynamic (#28394)
* Jira task: AECO-24
* Adding input type=password to AWS secrets fields (#28159)
* Galician onboarding translation (#27908 by @Thiagof99)

[2.38.1]
* Update Rocket.Chat to 6.2.9
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.2.9)
* 2f0f67f: fixed room-opened event not dispatching when navigating cached rooms
* 1ffc60d: fixed video message button disabled on iOS browsers
* 1ffc60d: fixed an error on mobile ios browser where if you started recording audio and denied permission, it would look like it is still recording 

[2.38.2]
* Update Rocket.Chat to 6.2.10
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.2.10)
* 2a09b64: fix: Prevent app's bridges from overriding the lastMsg prop which further was affecting Omni-Visitor abandonment feature for app
* ef4cd97: Fix Toggle message box formatting toolbar on click 

[2.38.3]
* Update Rocket.Chat to 6.2.11
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.2.11)
* Performance issue when using api to create users (#29914 by @KevLehman)

[2.39.0]
* Update Rocket.Chat to 6.3.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.0)
* Save deprecation usage on prometheus
* access-marketplace permission
* Add Apps engine Thread Bridge
* Add custom OAuth setting to allow merging users to others from distinct services
* Quick reactions on message toolbox
* Introduce Feature Preview page

[2.39.1]
* Update Rocket.Chat to 6.3.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.1)
* a874d5b: Translation files are requested multiple times
* cf9f16b: fix: Performance issue on Messages.countByType aggregation caused by unindexed property on messages collection
* be2b5c6: Bump @rocket.chat/meteor version.
* ce2f2ea: Added ability to freeze or completely disable integration scripts through envvars
* f29c326: fixed an issue where 2fa was not working after an OAuth redirect
* 09a24e5: Fix performance issue on Engagement Dashboard aggregation
* f29c326: fixed an issue where oauth login was not working with some providers

[2.39.2]
* Update Rocket.Chat to 6.3.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.2)
* 778b155ab4: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* 5660169ec8: fixed layout changing from embedded view when navigating
* f7b93f2a6a: Fixed an issue where timeout for http requests in Apps-Engine bridges was too short
* 653d97ce22: fix: mobile app unable to detect successful SAML login
* 8a0e36f7b1: fixed the video recorder window not closing after permission is denied. 

[2.39.3]
* Update Rocket.Chat to 6.3.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.3)
* c2fe38c: Added ability to disable private app installation via envvar `DISABLE_PRIVATE_APP_INSTALLATION`
* ded9666: Fix CORS headers not being set for assets

[2.39.4]
* Update Rocket.Chat to 6.3.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.4)
* db919f9b23: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* ebeb088441: fix: Prevent RoomProvider.useEffect from subscribing to room-data stream multiple times
* 8a7d5d3898: fix: agent role being removed upon user deactivation
* 759fe2472a: chore: Increase cache time from 5s to 10s on getUnits helpers. This should reduce the number of DB calls made by this method to fetch the unit limitations for a user.

[2.39.5]
* Update Rocket.Chat to 6.3.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.5)
* 4cb0b6b: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* f75564c: Fix a bug that prevented the error message from being shown in the private app installation page
* 0392340: Fixed selected departments not being displayed due to pagination
* 92d25b9: Change SAU aggregation to consider only sessions from few days ago instead of the whole past.

[2.39.6]
* Update Rocket.Chat to 6.3.6
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.6)
* 3bbe12e: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* 285e591: Fix engagement dashboard not showing data 

[2.39.7]
* Update Rocket.Chat to 6.3.7
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.7)
* e1acdda: User information crashing for some locales
* deffcb1: Increase cron job check delay to 1 min from 5s. This reduces MongoDB requests introduced on 6.3.

[2.39.8]
* Update Rocket.Chat to 6.3.8
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.3.8)
* ff8e9d9: Bump @rocket.chat/meteor version.

[2.40.0]
* Update Rocket.Chat to 6.4.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.0)
* 239a34e: new: ring mobile users on direct conference calls
* 04fe492: Added new Omnichannel's trigger condition "After starting a chat".
* 4186eec: Introduce the ability to report an user
* 92b690d: fix: Wrong toast message while creating a new custom sound with an existing name
* f83ea5d: Added support for threaded conversation in Federated rooms.
* 682d0bc: fix: Time format of Retention Policy
* 1b42dfc: Added a new Roles bridge to RC Apps-Engine for reading and retrieving role details.
* 2db32f0: Add option to select what URL previews should be generated for each message.
* 982ef6f: Add new event to notify users directly about new banners

[2.40.1]
* Update Rocket.Chat to 6.4.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.1)
* 636a412: fix: Remove model-level query restrictions for monitors
* 5e3473a: New setting to automatically enable autotranslate when joining rooms
* 2fa78b0: fix: Monitors now able to forward a chat without taking it first
* 0d14dc4: Add new permission to allow kick users from rooms without being a member

[2.40.2]
* Update Rocket.Chat to 6.4.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.2)
* eceeaf3b5d: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* 8e155d4212: fixed threads breaking when sending messages too fast
* 3f7ce23a90: fix: mobile ringing notification missing call id
* 36bcec8e40: Forward headers when using proxy for file uploads
* d20033723c: Handle the username update in the background

[2.40.3]
* Update Rocket.Chat to 6.4.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.3)
* a8676a3c5e: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* 69d89c4700: Fix unnecessary username validation on accounts profile form
* 02f4491d5a: fix: Omnichannel webhook is not retrying requests
* 02f4491d5a: Fixed intermittent errors caused by the removal of subscriptions and inquiries when lacking permissions.
* 25a2129beb: Rolled back a change of route from /admin/workspace to /admin/info
* 02f4491d5a: Add pagination & tooltips to agent's dropdown on forwarding modal
* 02f4491d5a: Added new Omnichannel setting 'Hide conversation after closing' 

[2.40.4]
* Update Rocket.Chat to 6.4.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.4)
* e832e9f: Fixed widget's nextAgent API sending an array of chars instead of an object for departmentId parameter
* a20c479: fix: custom-css injection
* 4789616: Fixed a problem that would prevent private apps from being shown on air-gapped environments

[2.40.5]
* Update Rocket.Chat to 6.4.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.5)

[2.40.6]
* Update Rocket.Chat to 6.4.6
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.6)
* 873eea9d54: fix: TypeError: Cannot use 'in' operator in undefined for every message sent
* b7ea8651bf: fix: immediate auto reload issues
* 35ea15005a: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* 57deb49ceb: fix: OAuth login by redirect failing on firefox
* 00875fc9ab: fix: wrong client hash calculation due to race condition on assets

[2.40.7]
* Update Rocket.Chat to 6.4.7
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.7)
* Add additional checks to the OAuth tokens to prevent future issues

[2.40.8]
* Update Rocket.Chat to 6.4.8
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.4.8)

[2.41.0]
* Update Rocket.Chat to 6.5.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.5.0)

[2.41.1]
* Update Rocket.Chat to 6.5.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.5.1)
* c2b224f: Security improvements
* c2b224f: Fixed issue with the new custom-roles license module not being checked throughout the application
* c2b224f: fix: stop refetching banner data each 5 minutes
* c2b224f: Fixed an issue allowing admin user cancelling subscription when license's trial param is provided
* c2b224f: Fixed Country select component at Organization form from onboarding-ui package
* c2b224f: fix Federation Regression, builds service correctly

[2.41.2]
* Update Rocket.Chat to 6.5.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.5.2)
* 84c4b07: Fixed conversations in queue being limited to 50 items
* 886d920: Fix wrong value used for Workspace Registration 

[2.41.3]
* Update Rocket.Chat to 6.5.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.5.3)
* b1e72a84d9: Fix user being logged out after using 2FA
* de2658e874: Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* 2a04cc850b: fix: multiple indexes creation error during 304 migration 

[2.41.4]
* Migrate LDAP users from user ID to username

[2.42.0]
* Migrate to OIDC login

[2.43.0]
* Update Rocket.Chat to 6.6.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.0)
* (#31184) Add the possibility to hide some elements through postMessage events.
* (#31516) Room header keyboard navigability
* (#30868) Added push.info endpoint to enable users to retrieve info about the workspace's push gateway
* (#31510) Composer keyboard navigability
* (#30464) Mentioning users that are not in the channel now dispatches a warning message with actions
* (#31369) feat: add ImageGallery zoom controls
* (#31393 by @hardikbhatia777) Fixes an issue where avatars are not being disabled based on preference on quote attachments
* (#30680) feat: Skip to main content shortcut and useDocumentTitle
* (#31299) fix: Loading state for Marketplace related lists

[2.43.1]
* Update Rocket.Chat to 6.6.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.1)
* (#31713) Fixes an issue not allowing admin users to edit the room name
* (#31723) fixed an issue with the user presence not updating automatically for other users.
* (#31753) Fixed an issue where the login button for Custom OAuth services would not work if any non-custom login service was also available
* (#31554 by @shivang-16) Fixed a bug on the rooms page's "Favorite" setting, which previously failed to designate selected rooms as favorites by default.

[2.43.2]
* Update Rocket.Chat to 6.6.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.2)
* Bump @rocket.chat/meteor version.
* Bump @rocket.chat/meteor version.
* (#31844) Fixed Federation not working with Microservice deployments
* (#31823) Revert unintentional changes real time presence data payload
* (#31833) Fix web UI not showing users presence updating to offline

[2.43.3]
* Update Rocket.Chat to 6.6.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.3)
* (#31895) Fix users presence stuck as online after connecting using mobile apps

[2.43.4]
* Update Rocket.Chat to 6.6.4
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.4)
* (#31700) Fixed matrix homeserver domain setting not being visible in admin panel
* (#32012) Don't use the registration.yaml file to configure Matrix Federation anymore.
* (#31927) stopped lifecycle method was unexpectedly synchronous when using microservices, causing our code to create race conditions.

[2.43.5]
* Update Rocket.Chat to 6.6.5
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.5)
* (#31998) Introduced a new step to the queue worker: when an inquiry that's on an improper status is selected for processing, queue worker will first check its status and will attempt to fix it.
* This prevents issues where the queue worker attempted to process an inquiry forever because it was in an improper state.

[2.43.6]
* Update Rocket.Chat to 6.6.6
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.6.6)
* (#32064) Fix an issue affecting Rocket.Chat Apps utilizing the OAuth 2 library from Apps Engine, ensuring that apps like Google Drive and Google Calendar are operational once more.
* (#32056) Fix error during migration 304. Throwing Cannot read property 'finally' of undefined error.

[2.44.0]
* Update Rocket.Chat to 6.7.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.7.0)

[2.44.1]
* Update Rocket.Chat to 6.7.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.7.1)
* (#32253) Increased the timeout between calls for the three remaining Omnichannel Agenda Jobs. This should make them happen less often and reduce the load on MongoDB
* (#32252) Fixes an issue that forces the focus on the last message when interacting by mouse on message list
* (#32256) Fixed open expanded view (galery mode) for image attachments sent by livechat widget
* (#32254) Fixed an issue where Rocket.Chat would ask admins to confirm fingerprint change (new workspace vs configuration update), even when AUTO_ACCEPT_FINGERPRINT environment variable set to "true".
* (#32265) Fixed supported versions not being updated in airgapped environments
* (#32251) Fixes an issue where message reactions are vertically misaligned when zooming out

[2.44.2]
* Update Rocket.Chat to 6.7.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.7.2)
* (#32315 by @dionisio-bot) fixed Engagement Dashboard and Device Management admin pages loading indefinitely

[2.45.0]
* Update Rocket.Chat to 6.8.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.8.0)
* (#31898) Created a new endpoint to get a filtered and paginated list of users.
* (#32224) Allow Custom Fields in Messages. API-only feature. It can be enabled and configured in Workspace Settings.
* (#32115) Introduces sidebar navigability, allowing users to navigate on sidebar channels through keyboard
* (#29461) Introduces a resizable Contextualbar allowing users to change the width just by dragging it
* (#31811) Convert mute/unmute meteor methods to endpoints
* (#32084) Added a new setting to automatically disable users from LDAP that can no longer be found by the background sync
* (#31965) Added the ability to serve .well-known paths directly from Rocket.Chat, if using federation, removing the need for special reverse proxy configuration or another component layer for specific types of reverse proxies / loadbalancers.
* (#31898) Created a new endpoint to resend the welcome email to a given user
* (#32208) Added a new notification provider in light of the old FCM API deprecation, now you can choose to use the new provider or the old via the Push_UseLegacy setting
* (#31976) Added support for allowing agents to forward inquiries to departments that may not have any online agents given that Allow department to receive forwarded inquiries even when there's no available agents is set to true in the department configuration.
* This configuration empowers agents to seamlessly direct incoming requests to the designated department, ensuring efficient handling of queries even when departmental resources are not actively online. When an agent becomes available, any pending inquiries will be automatically routed to them if the routing algorithm supports it.
* (#32173) Added "Enable Users" option under "Sync User Active State" LDAP setting to allow only re-enabling users found on LDAP background sync
* (#31865) Redesign Save E2EE password modal
* (#32272) Support Message Custom Fields on upload API via field customField and JSON value
* (#32055) feat: ConnectionStatusBar redesign
* (#32073) Fixed an issue affecting the update modal/contextual bar by apps when it comes to error handling and regular surface update

[2.46.0]
* Update Rocket.Chat to 6.9.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.9.0)
* (#31917) Introduced a tab layout to the users page and implemented a tab called "All" that lists all users.
* (#32439) Allow visitors & integrations to access downloaded files after a room has closed. This was a known limitation in our codebase, where visitors were only able to access uploaded files in a livechat conversation while the conversation was open.
* (#32233) Makes the triggers fired by the condition after-guest-registration persist on the livechat client, it will persist through reloads and pagination, only reseting when a conversation is closed (no changes were done on the agent side of the conversation)
* (#32193) Adds CheckOption to departments multi selects improving options visibility state
* (#32317) Replace the read receipt receipt indicator in order to improve the accessibility complience
* (#32341) Changes the scrollbar color in order to improve the contrast and accessibility compliance
* (#32298) Added "Rocket.Chat Cloud Workspace ID" to workspace statistics page

[2.46.1]
* Update Rocket.Chat to 6.9.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.9.1)
* Fixes issues with loading license modules when loading the page while logged out
* Fixes issues causing nonstop sound notification when taking a chat from the Current Chats view

[2.46.2]
* Update Rocket.Chat to 6.9.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.9.2)

[2.46.3]
* Update Rocket.Chat to 6.9.3
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.9.3)

[2.47.0]
* Update Rocket.Chat to 6.10.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.10.0)

[2.47.1]
* Update Rocket.Chat to 6.10.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.10.1)

[2.47.2]
* Update Rocket.Chat to 6.10.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.10.2)

[2.48.0]
* Update Rocket.Chat to 6.11.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.11.0)

[2.48.1]
* Update Rocket.Chat to 6.11.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.11.1)

[2.48.2]
* Update Rocket.Chat to 6.11.2
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.11.2)
* (#33084 by @dionisio-bot) Prevent processRoomAbandonment callback from erroring out when a room was inactive during a day Business Hours was not configured for.
* (#33153 by @dionisio-bot) Security Hotfix (https://docs.rocket.chat/docs/security-fixes-and-updates)
* (#33185 by @dionisio-bot) Restored tooltips to the unit edit department field selected options
* (#33129 by @dionisio-bot) Fixed an issue related to setting Accounts_ForgetUserSessionOnWindowClose, this setting was not working as expected.
* (#33178 by @dionisio-bot) Fixes an issue where multi-step modals were closing unexpectedly

[2.49.0]
* Update Rocket.Chat to 6.12.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.12.0)
* (#32535) Federation actions like sending message in a federated DM, reacting in a federated chat, etc, will no longer work if the configuration is invalid.
* (#32916) Added a new Audit endpoint audit/rooms.members that allows users with view-members-list-all-rooms to fetch a list of the members of any room even if the user is not part of it.
* (#32032) Added a new 'Deactivated' tab to the users page, this tab lists users who have logged in for the first time but have been deactivated for any reason. Also added the UI code for the Active tab;
* (#33044) Replaces an outdated banner with the Bubble component in order to display retention policy warning
* (#32867) Added an accordion for advanced settings on Create teams and channels
* (#32709 by @heet434) Add "Created at" column to admin rooms table
* (#32535) New button added to validate Matrix Federation configuration. A new field inside admin settings will reflect the configuration status being either 'Valid' or 'Invalid'.
* (#32969) Upgrades fuselage-toastbar version in order to add pause on hover functionality
* (#33003) Added a new setting to enable/disable file encryption in an end to end encrypted room.
* (#32868) Added sidepanel field to teams.create and rooms.saveRoomSettings endpoints
* (#33003) Fixed a bug related to uploading end to end encrypted file.

[2.49.1]
* Update Rocket.Chat to 6.12.1
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.12.1)
* (#33242 by @dionisio-bot) Allow to use the token from room.v when requesting transcript instead of visitor token. Visitors may change their tokens at any time, rendering old conversations impossible to access for them (or for APIs depending on token) as the visitor token won't match the room.v token.
* (#33268 by @dionisio-bot) Security Hotfix (https://docs.rocket.chat/docs/security-fixes-and-updates)
* (#33265 by @dionisio-bot) fixed retention policy max age settings not being respected after upgrade

[2.50.0]
* Update Rocket.Chat to 6.13.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/6.13.0)
* (#33156) added sidepanelNavigation to feature preview list
* (#32682) Added support for specifying a unit on departments' creation and update
* (#33139) Added new setting Allow visitors to finish conversations that allows admins to decide if omnichannel visitors can close a conversation or not. This doesn't affect agent's capabilities of room closing, neither apps using the livechat bridge to close rooms.
* However, if currently your integration relies on livechat/room.close endpoint for closing conversations, it's advised to use the authenticated version livechat/room.closeByUser of it before turning off this setting.
* (#32729) Implemented "omnichannel/contacts.update" endpoint to update contacts
* (#32510) Added a new setting to enable mentions in end to end encrypted channels
* (#32821) Replaced new SidebarV2 components under feature preview
* (#33212) Added new Admin Feature Preview management view, this will allow the workspace admins to both enable feature previewing in the workspace as well as define which feature previews are enabled by default for the users in the workspace.
* (#33011) Return parent and team information when calling rooms.info endpoint
* (#32693) Introduced "create contacts" endpoint to omnichannel
* (#33177) New teams.listChildren endpoint that allows users listing rooms & discussions from teams. Only the discussions from the team's main room are returned.
* (#33114) Wraps some room settings in an accordion advanced settings section in room edit contextual bar to improve organization
* (#33160) Implemented sending email via apps
* (#32945) Added a new setting which allows workspace admins to disable email two factor authentication for SSO (OAuth) users. If enabled, SSO users won't be asked for email two factor authentication.
* (#33225) Implemented new feature preview for Sidepanel


[2.51.0]
* Update Rocket.Chat to 7.0.0
* [Full changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.0.0)
[2.52.0]
* Update Rocket.Chat to 7.1.0
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.1.0)
* ([#&#8203;33897](https://github.com/RocketChat/Rocket.Chat/pull/33897)) adds unread badge to sidebar collapser
* ([#&#8203;32906](https://github.com/RocketChat/Rocket.Chat/pull/32906)) Improves thread metrics featuring user avatars, better titles and repositioned elements.
* ([#&#8203;33810](https://github.com/RocketChat/Rocket.Chat/pull/33810)) Adds cursor pagination on chat.syncMessages endpoint
* ([#&#8203;33214](https://github.com/RocketChat/Rocket.Chat/pull/33214)) Adds a new route to allow fetching avatars by the user's id `/avatar/uid/<UserID>`
* ([#&#8203;32727](https://github.com/RocketChat/Rocket.Chat/pull/32727)) These changes aims to add:
* ([#&#8203;33920](https://github.com/RocketChat/Rocket.Chat/pull/33920)) Improves the customizability of the naming of automatic Persistent video calls discussions, allowing the date of the call to be in different parts of the name, using the `[date]` keyword.
* ([#&#8203;33997](https://github.com/RocketChat/Rocket.Chat/pull/33997)) Prevent apps' subprocesses from crashing on unhandled rejections or uncaught exceptions
* ([#&#8203;33814](https://github.com/RocketChat/Rocket.Chat/pull/33814)) Adds a confirmation modal to the cancel subscription action
* ([#&#8203;33776](https://github.com/RocketChat/Rocket.Chat/pull/33776)) Fix user highlights not matching only whole words
* ([#&#8203;33818](https://github.com/RocketChat/Rocket.Chat/pull/33818)) Remove unused client side `setUserActiveStatus` meteor method.
* ([#&#8203;33596](https://github.com/RocketChat/Rocket.Chat/pull/33596)) Bump meteor to 3.0.4 and Node version to 20.18.0
* ([#&#8203;33713](https://github.com/RocketChat/Rocket.Chat/pull/33713)) Deprecated the `from` field in the apps email bridge and made it optional, using the server's settings when the field is omitted
* ([#&#8203;32991](https://github.com/RocketChat/Rocket.Chat/pull/32991)) Fixes an issue where updating custom emojis didnt work as expected, ensuring that uploaded emojis now update correctly and display without any caching problems.
* ([#&#8203;33985](https://github.com/RocketChat/Rocket.Chat/pull/33985)) Fixes issue that could cause multiple discussions to be created when creating it from a message action
* ([#&#8203;33904](https://github.com/RocketChat/Rocket.Chat/pull/33904)) adds missing html attributes in sidebar item templates
* ([#&#8203;33218](https://github.com/RocketChat/Rocket.Chat/pull/33218)) Fixes message character limit not being applied to file upload descriptions
* ([#&#8203;33908](https://github.com/RocketChat/Rocket.Chat/pull/33908)) Fixes the issue where newly created teams are incor

[2.53.0]
* Update Rocket.Chat to 7.2.0
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.2.0)
* ([#&#8203;34194](https://github.com/RocketChat/Rocket.Chat/pull/34194)) Adds a new `contacts.checkExistence` endpoint, which allows identifying whether there's already a registered contact using a given email, phone, id or visitor to source association.
* ([#&#8203;34004](https://github.com/RocketChat/Rocket.Chat/pull/34004)) Allows Rocket.Chat to store call events.
* ([#&#8203;33895](https://github.com/RocketChat/Rocket.Chat/pull/33895)) Adds statistics related to the new **Contact Identification** feature:
* ([#&#8203;34220](https://github.com/RocketChat/Rocket.Chat/pull/34220)) Disables OTR messages selection when exporting messages
* ([#&#8203;34121](https://github.com/RocketChat/Rocket.Chat/pull/34121)) Organizes App Settings interface by introducing section-based accordion groups to improve navigation and readability for administrators.
* ([#&#8203;34076](https://github.com/RocketChat/Rocket.Chat/pull/34076)) Introduces a new option when exporting messages, allowing users to select and download a JSON file directly from client
* ([#&#8203;34057](https://github.com/RocketChat/Rocket.Chat/pull/34057)) Improves the workspace and subscription admin pages by updating font scaling, centralizing elements,
* ([#&#8203;33549](https://github.com/RocketChat/Rocket.Chat/pull/33549)) Adds a new callout in the subscription page to inform users of subscription upgrade eligibility when applicable.
* ([#&#8203;34205](https://github.com/RocketChat/Rocket.Chat/pull/34205)) Fixes an error where the engine would not retry a subprocess restart if the last attempt failed
* ([#&#8203;34137](https://github.com/RocketChat/Rocket.Chat/pull/34137)) Fixes Unit's `numDepartments` property not being updated after a department is removed

[2.54.0]
* Checklist added to CloudronManifest
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[2.54.1]
* Use Deno v1.37.1 to fix rocketchat apps

[2.54.2]
* Update Rocket.Chat to 7.2.1
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.2.1)
* Node: `20.18.0`
* MongoDB: `5.0, 6.0, 7.0`
* Apps-Engine: `1.48.1`
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.

[2.54.3]
* set the deno cache dir

[2.55.0]
* Update Rocket.Chat to 7.3.0
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.3.0)
* ([#&#8203;33060](https://github.com/RocketChat/Rocket.Chat/pull/33060)) Added `departmentsAllowedToForward` property to departments returned in the `livechat/config` endpoint
* ([#&#8203;34153](https://github.com/RocketChat/Rocket.Chat/pull/34153)) Groups members by their roles in the room's member list for improved clarity
* ([#&#8203;34940](https://github.com/RocketChat/Rocket.Chat/pull/34940)) Allows agents and managers to close Omnichannel rooms that for some reason ended up in a bad state. This "bad state" could be a room that appears open but it's closed. Now, the endpoint `livechat/room.closeByUser` will accept an optional `forceClose` parameter that will allow users to bypass most state checks we do on rooms and perform the room closing again so its state can be recovered.
* ([#&#8203;34948](https://github.com/RocketChat/Rocket.Chat/pull/34948)) Adds voice calls data to statistics
* ([#&#8203;34922](https://github.com/RocketChat/Rocket.Chat/pull/34922)) Fixes an issue where users without the "Preview public channel" permission would receive new messages sent to the channel
* ([#&#8203;34191](https://github.com/RocketChat/Rocket.Chat/pull/34191)) Allows granting the `mobile-upload-file` permission to guests
* ([#&#8203;34153](https://github.com/RocketChat/Rocket.Chat/pull/34153)) Adds `rooms.membersOrderedByRole` endpoint to retrieve members of groups and channels sorted according to their respective role in the room.
* ([#&#8203;34904](https://github.com/RocketChat/Rocket.Chat/pull/34904)) Security Hotfix (https://docs.rocket.chat/docs/security-fixes-and-updates)
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* ([#&#8203;34846](https://github.com/RocketChat/Rocket.Chat/pull/34846)) Fixes the send attachments option not working on iOS browsers by moving it from the composer dropdown menu to the composer primary actions.
* ([#&#8203;34858](https://github.com/RocketChat/Rocket.Chat/pull/34858)) Fixes an issue that prevented the apps-engine from reestablishing communications with subprocesses in some cases
* ([#&#8203;34849](https://github.com/RocketChat/Rocket.Chat/pull/34849)) Fixes an issue where losing connection could break app's action buttons
* ([#&#8203;35010](https://github.com/RocketChat/Rocket.Chat/pull/35010)) Prevent a bug that caused all sessions being marked as logged out if some required value was missing due to a race condition.
* ([#&#8203;34864](https://github.com/RocketChat/Rocket.Chat/pull/34864)) Allows users to fetch the `packageValue` of settings when calling `/settings` endpoint via `includeDefaults` query param.
* ([#&#8203;34908](https://github.com/RocketChat/Rocket.Chat/pull/34908)) Fixes an issue where room scroll position wasn't being restored when moving between rooms.
* ([#&#8203;34887](https://github.com/RocketChat/Rocket.Chat/pull/34887)) Fixes an issue where the system would throw the error: 'GUI Application error' while uninstalling an app(when on the requests tab).
* ([#&#8203;34900](https://github.com/RocketChat/Rocket.Chat/pull/34900)) Fixes an issue that prevented room history from loading under certain conditions.
* ([#&#8203;35009](https://github.com/RocketChat/Rocket.Chat/pull/35009)) Fix an issue with apps installations via Marketplace
* ([#&#8203;34503](https://github.com/RocketChat/Rocket.Chat/pull/34503)) Fixes an issue where changing user role in a room displays the wrong message
* ([#&#8203;34873](https://github.com/RocketChat/Rocket.Chat/pull/34873)) Fixes SAML login redirecting to wrong room when using an invite link.
* ([#&#8203;34839](https://github.com/RocketChat/Rocket.Chat/pull/34839)) Fixes apps actions showing in toolbar without an icon
* ([#&#8203;34892](https://github.com/RocketChat/Rocket.Chat/pull/34892)) Fixes an issue where Rocket cat would send '%s' as the mongo version when using a deprecated but supported version of MongoDB.
* ([#&#8203;34860](https://github.com/RocketChat/Rocket.Chat/pull/34860)) Fixes an issue where room's file list would incorrectly display incoming messages as files.
* ([#&#8203;34210](https://github.com/RocketChat/Rocket.Chat/pull/34210)) Fixes livechat conversations not being assigned to the contact manager even when the "Assign new conversations to the contact manager" setting is enabled
* ([#&#8203;34113](https://github.com/RocketChat/Rocket.Chat/pull/34113)) Fixes dates being incorrect when fetching omnichannel reports with the following time periods:
* ([#&#8203;35055](https://github.com/RocketChat/Rocket.Chat/pull/35055)) Fixes an issue where the container image could not be run as any random non-root user id.
* ([#&#8203;34935](https://github.com/RocketChat/Rocket.Chat/pull/34935)) Fixes an issue with the retention policy max age settings not maintaning it's previous value when upgrading from version < 6.10
* ([#&#8203;34933](https://github.com/RocketChat/Rocket.Chat/pull/34933)) Fixes SlackBridge service failing to connect to multiple slack workspaces
* ([#&#8203;34851](https://github.com/RocketChat/Rocket.Chat/pull/34851)) Fixes thumbnails not being deleted from storage on room deletion
* <details><summary>Updated dependencies [79cba772bd8ae0a1e084687b47e05f312e85078a, c8e8518011b8b7d318a2bb2f26b897b196421d76, 5506c406f4a22145ece065ad2b797225e94423ca, c75d771c410579d3d7eaabb379871456ded1b111, 8942b0032af976738a7c602fa389803dda30c0dc, 4aa95b61edaf6ce4fe0c5bdbc3d0157bf3d6794b, 1f54b733eaa91e602baaff74f113c7ef16ddaa89, bfa92f4dba1a16973d7da5a9c0f5d0df998bf944, c0fa1c884cccab47f4e68dd81457c424cf176f11, 3c237b25b27782db2e54c4c294140b1c8cd9b31a, b4ce5797b7fc52e851aa4afc54c4617fc12cbf72, c8e8518011b8b7d318a2bb2f26b897b196421d76]:</summary>

[2.55.1]
* Update Rocket.Chat to 7.3.1
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.3.1)
* Node: `22.11.0`
* MongoDB: `5.0, 6.0, 7.0`
* Apps-Engine: `1.48.2`
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* ([#&#8203;35112](https://github.com/RocketChat/Rocket.Chat/pull/35112) by [@&#8203;dionisio-bot](https://github.com/dionisio-bot)) Fixes the queue processing of Omnichannel's waiting queue focusing on 3 main areas:
* ([#&#8203;35096](https://github.com/RocketChat/Rocket.Chat/pull/35096) by [@&#8203;dionisio-bot](https://github.com/dionisio-bot)) Fixes a behavior in Omnichannel that was causing bot agents to be waiting in the queue, when they should always skip it.
* <details><summary>Updated dependencies [b7905dfebe48d27d0d774fb23cc579ea9dfd01f4]:</summary>

[2.55.2]
* Update Rocket.Chat to 7.3.2
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.3.2)
* Node: `22.11.0`
* MongoDB: `5.0, 6.0, 7.0`
* Apps-Engine: `1.48.2`
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* ([#&#8203;35212](https://github.com/RocketChat/Rocket.Chat/pull/35212) by [@&#8203;dionisio-bot](https://github.com/dionisio-bot)) Fixes incorrect start date on omnichannel reports
* ([#&#8203;35222](https://github.com/RocketChat/Rocket.Chat/pull/35222) by [@&#8203;dionisio-bot](https://github.com/dionisio-bot)) Fixes `channels.list` endpoint from rejecting pagination parameters
* ([#&#8203;35251](https://github.com/RocketChat/Rocket.Chat/pull/35251) by [@&#8203;dionisio-bot](https://github.com/dionisio-bot)) fixes an issue with embedded layout rooms displaying as if the user is not part of the room
* <details><summary>Updated dependencies []:</summary>

[2.66.0]
* Use the correct base image 5

[2.66.1]
* Update Rocket.Chat to 7.3.3
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.3.3)
* Node: `22.11.0`
* MongoDB: `5.0, 6.0, 7.0`
* Apps-Engine: `1.48.2`
* Bump [@&#8203;rocket](https://github.com/rocket).chat/meteor version.
* Add a retry mechanism to get supported versions from Cloud
* ([#&#8203;35353](https://github.com/RocketChat/Rocket.Chat/pull/35353) by [@&#8203;dionisio-bot](https://github.com/dionisio-bot)) Fixes omnichannel transcript filename breaking download links
* <details><summary>Updated dependencies [b2d71461a6a73157024e4594cc1228419a34673e]:</summary>

[2.67.0]
* Update Rocket.Chat to 7.4.0
* [Full Changelog](https://github.com/RocketChat/Rocket.Chat/releases/tag/7.4.0)
* Node: `22.13.1`
* MongoDB: `5.0, 6.0, 7.0`
* Apps-Engine: `1.49.0`
* ([#&#8203;34208](https://github.com/RocketChat/Rocket.Chat/pull/34208)) Adds a new endpoint `rooms.hide` to hide rooms of any type when provided with the room's ID
* ([#&#8203;35147](https://github.com/RocketChat/Rocket.Chat/pull/35147)) Allows users to filter by multiple departments & by livechat units on `livechat/rooms` endpoint.
* ([#&#8203;34274](https://github.com/RocketChat/Rocket.Chat/pull/34274)) Adds a new setting that if enabled, will not count bot messages in the average response time metrics
* ([#&#8203;35177](https://github.com/RocketChat/Rocket.Chat/pull/35177)) Adds a new IPostSystemMessageSent event, that is triggered whenever a new System Message is sent
* ([#&#8203;34957](https://github.com/RocketChat/Rocket.Chat/pull/34957)) Implements a modal to let users know about VoIP calls in direct messages and missing configurations.
* ([#&#8203;34958](https://github.com/RocketChat/Rocket.Chat/pull/34958)) Makes Omnichannel converstion start process transactional.
* ([#&#8203;34926](https://github.com/RocketChat/Rocket.Chat/pull/34926)) Enables control of video conference ringing and dialing sounds through the call ringer volume user preference, preventing video conf calls from always playing at maximum volume.
* ([#&#8203;35260](https://github.com/RocketChat/Rocket.Chat/pull/35260)) Enhances message sorting in the `im.messages` and `dm.messages` endpoints by enabling support for multi-parameter sorting.

