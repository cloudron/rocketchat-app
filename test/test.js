#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_MESSAGE = 'Hello Test!';
    const TEST_CHANNEL = 'general';

    const TEST_TIMEOUT = 60000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let uploadedImageUrl = '';
    const username = process.env.USERNAME, password = process.env.PASSWORD;
    let REGISTRATION_EMAIL = process.env.REGISTRATION_EMAIL || 'registration@cloudron.io';
    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function setupWizard() {
        await browser.get(`https://${app.fqdn}/setup-wizard/1`);
        await waitForElement(By.xpath('//input[@name="fullname"]'));
        await browser.findElement(By.xpath('//input[@name="fullname"]')).sendKeys('RocketChat Admin');
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys('rcadmin');
        await browser.findElement(By.xpath('//input[@name="email"]')).sendKeys(REGISTRATION_EMAIL);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys('changeme');
        await browser.findElement(By.xpath('//button/span[text()="Next"]/..')).click();

        await waitForElement(By.xpath('//input[@name="organizationName"]'));

        // now skip it
        execSync(`cloudron exec --app ${app.id} -- bash -c 'mongosh -u $CLOUDRON_MONGODB_USERNAME -p $CLOUDRON_MONGODB_PASSWORD $CLOUDRON_MONGODB_HOST:$CLOUDRON_MONGODB_PORT/$CLOUDRON_MONGODB_DATABASE --eval "db.rocketchat_settings.updateOne({ _id: \\\"Show_Setup_Wizard\\\" }, { \\\$set: {value: \\\"completed\\\"} })"'`);

        // let it sink in
        await browser.sleep(5000);
    }

    async function login(emailOrUsername) {
        await browser.get(`https://${app.fqdn}/home`);
        await browser.sleep(5000);
        await waitForElement(By.xpath('//input[@name="username"]'));
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(emailOrUsername);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button/span[text()="Login"]/..')).click();
        await waitForElement(By.xpath('//img[contains(@src, "/avatar/")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/home`);
        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(., "Login with")]'));
        await browser.findElement(By.xpath('//button[contains(., "Login with")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//img[contains(@src, "/avatar/")]'));
    }

    async function logout(username) {
        await browser.get(`https://${app.fqdn}/home`);
        await browser.wait(until.elementLocated(By.xpath(`//img[contains(@src, "/avatar/${username}")]`)), TEST_TIMEOUT);
        await browser.findElement(By.xpath(`//img[contains(@src, "/avatar/${username}")]`)).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//div[text()="Logout"]'));
        await browser.findElement(By.xpath('//div[text()="Logout"]')).click();
        await browser.sleep(3000);
    }

    async function sendMessage() {
        await browser.get(`https://${app.fqdn}/channel/${TEST_CHANNEL}`);
        await waitForElement(By.name('msg'));
        await browser.findElement(By.name('msg')).sendKeys(TEST_MESSAGE);
        await browser.findElement(By.name('msg')).sendKeys(Key.RETURN);
        await waitForElement(By.xpath('//*[text()=\'' + TEST_MESSAGE + '\']'));
    }

    async function checkMessage() {
        await browser.get(`https://${app.fqdn}/channel/${TEST_CHANNEL}`);
        await browser.sleep(4000);
        await waitForElement(By.name('msg'));
        await browser.sleep(10000);
        await waitForElement(By.xpath('//*[text()=\'' + TEST_MESSAGE + '\']'));
    }

    async function joinChannel() {
        await browser.get(`https://${app.fqdn}/channel/${TEST_CHANNEL}`);
        await waitForElement(By.name('msg'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can pass setup wizard', setupWizard);
    it('can logout', logout.bind(null, 'rcadmin'));

    it('can login via OIDC', loginOIDC.bind(null, username, password, false));
    it('can join channel', joinChannel);
    it('can send message', sendMessage);
    it('can logout', logout.bind(null, username));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get(`https://${app.fqdn}/home`);
        await browser.executeScript('localStorage.clear();');
        await browser.get('about:blank');

        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('message is still there', checkMessage);
    it('can logout', logout.bind(null, username));

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('message is still there', checkMessage);

    it('move to different location', async function () {
        await browser.get(`https://${app.fqdn}/home`);
        await browser.executeScript('localStorage.clear();');

        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
        uploadedImageUrl = uploadedImageUrl.replace(LOCATION, LOCATION + '2');
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('message is still there', checkMessage);
    it('can logout', logout.bind(null, username));

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install previous version from appstore', function () { execSync(`cloudron install --appstore-id chat.rocket.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can pass setup wizard', setupWizard);
    it('can logout', logout.bind(null, 'rcadmin'));

    it('can login via OIDC', loginOIDC.bind(null, username, password));
    it('can join channel', joinChannel);
    it('can send message', sendMessage);
    it('can logout', logout.bind(null, username));

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('message is still there', checkMessage);
    it('can send message', sendMessage);
    it('message is still there', checkMessage);

    it('can logout', logout.bind(null, username));

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
