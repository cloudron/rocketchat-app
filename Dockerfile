FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

# https://github.com/RocketChat/Docker.Official.Image/blob/main/7.2/Dockerfile#L3
ARG DENO_VERSION=1.37.1
RUN wget -O /usr/bin/deno.zip https://github.com/denoland/deno/releases/download/v${DENO_VERSION}/deno-x86_64-unknown-linux-gnu.zip && \
  cd /usr/bin && \
  unzip deno.zip && \
  rm deno.zip

RUN node -v | grep -q 'v22' || (echo "Requires node v22" && exit 1)

# renovate: datasource=github-releases depName=RocketChat/Rocket.Chat versioning=semver
ARG ROCKETCHAT_VERSION=7.4.0

# https://releases.rocket.chat/${ROCKETCHAT_VERSION}/download
RUN curl -SLf "https://cdn-download.rocket.chat/build/rocket.chat-${ROCKETCHAT_VERSION}.tgz" | tar -zxf - -C /app/code \
  && cd /app/code/bundle/programs/server \
  && npm install \
  && chown -R cloudron:cloudron /app/code

# there is really strange oplog parsing bug where if the authSource starts with a number, it doesn't parse correctly
RUN sed -e "s,useNewUrlParser: true,useNewUrlParser: false," -i /app/code/bundle/programs/server/packages/mongo.js

# For some reason, setting BABEL_CACHE_PATH env var doesn't work
RUN ln -s /run/rocket.chat/babel-cache /home/cloudron/.babel-cache

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
